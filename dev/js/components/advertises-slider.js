window.cust.advertises_slider.slick({
	dots: false,
	infinite: false,
	speed: 300,
	slidesToShow: 4,
	slidesToScroll: 1,
	adaptiveHeight: true,
	prevArrow: '<button type="button" class="slick-prev"></button>',
	nextArrow: '<button type="button" class="slick-next"></button>',
	responsive: [
		{
			breakpoint:window.cust.bp_SM,
			settings: {
				slidesToShow: 3
			}
		},
		{
			breakpoint: window.cust.bp_XS,
			settings: {
				slidesToShow: 2
			}
		},
		{
			breakpoint: window.cust.bp_XX,
			settings: {
				slidesToShow: 1
			}
		}
 	]
});