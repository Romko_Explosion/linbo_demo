$('body').on('click', '.js--phone-trig', function(e) {
	var number_item = $(this)
		.closest('.js--phone-wrap')
		.find('.js--phone-number');
	number_item.text(number_item.attr('data-original-number'));
	e.preventDefault();
})