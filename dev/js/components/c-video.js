$('body').on('click', '.c-video__item-play', function (e) {
	th = $(this),
	th_item = th.closest('.c-video__item');
	stop_all_video();
	th_item.addClass('is-active');
	th_item.find('.c-video__item-iframe').attr('src', th.attr('href') + '?autoplay=1');

	e.preventDefault();
});

function stop_all_video() {
	var video_items = $('.c-video__item');
	video_items
		.removeClass('is-active')
		.find('.c-video__item-iframe')
		.attr('src','javascript:void(0)');
};