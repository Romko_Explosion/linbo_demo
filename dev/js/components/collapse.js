var $body = $('body');
if($('.collapse').length > 0){
	require(['bs_transition', 'bs_collapse'], function(){
		$('.panel-heading a').on('click', function(event){
			var th = $(this);
			var all = th.closest('.panel-group').find('.collapse');
			var current = th.closest('.panel').find('.collapse');
			all.not(current).collapse('hide').prev('.panel-heading').children('a').addClass('collapsed');
			current.collapse('toggle').prev('.panel-heading').children('a').toggleClass('collapsed');
			event.preventDefault();
		});
		$body.on('resize_xx resize_xs resize_sm_once', function(e){
			$('.collapse-group .collapse').collapse('hide').removeClass('in').prev('.panel-heading').children('a').addClass('collapsed');
		});
		$body.on('resize_md_once resize_ml_once resize_lg_once', function(e){
			$('.collapse-group .collapse.opened_default').collapse('show').addClass('in').prev('.panel-heading').children('a').removeClass('collapsed');
		});
		window.cust.resizer();
	});
};