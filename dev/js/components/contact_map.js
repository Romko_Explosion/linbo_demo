function init_contact_map(){
	var coords = [
		parseFloat(window.cust.contact_map.attr('data-lat')),
		parseFloat(window.cust.contact_map.attr('data-lng'))
	];

	var contact_map = new ymaps.Map(window.cust.contact_map[0], {
		center: coords,
		zoom: 12,
		controls: []
	});

	contact_map.behaviors.disable('scrollZoom');

	var contact_map_collection = new ymaps.GeoObjectCollection({}, {
		iconImageHref: 's/images/useful/0.png',
		iconLayout : 'default#imageWithContent',
		iconImageSize: [20, 20],
		iconOffset: [3, 0]
	});

	contact_map_collection.add( new ymaps.Placemark( coords,
		{
			iconContent: '<div class="map-point"></div>'
		}
	));

	contact_map.geoObjects.add(contact_map_collection);

	$(document).on('resize', function(){
		contact_map.container.fitToViewport();
	});
};
init_contact_map();