window.cust.detail_slider.slick({
	dots: false,
	infinite: false,
	speed: 300,
	slidesToShow: 1,
	slidesToScroll: 1,
	adaptiveHeight: true,
	asNavFor:'.c-detail__nav-slider',
	prevArrow: '<button type="button" class="slick-prev"></button>',
	nextArrow: '<button type="button" class="slick-next"></button>'
});
window.cust.detail_nav_slider.slick({
	dots: false,
	infinite: false,
	arrows:false,
	speed: 300,
	slidesToShow: 6,
	slidesToScroll: 1,
	asNavFor:'.c-detail__slider',
	responsive: [
		{
			breakpoint:window.cust.bp_MD,
			settings: {
				slidesToShow: 5
			}
		},
		{
			breakpoint:window.cust.bp_SM,
			settings: {
				slidesToShow: 6
			}
		},
		{
			breakpoint: window.cust.bp_XS,
			settings: {
				slidesToShow: 5
			}
		},
		{
			breakpoint: window.cust.bp_XX,
			settings: {
				slidesToShow: 4
			}
		}
 	]
});
window.cust.detail_nav_slider.on('click','.c-detail__nav-slide a', function (e) {
	window.cust.detail_slider.slick('slickGoTo',parseInt($(this).closest('.slick-slide').index()));
	e.preventDefault();
});