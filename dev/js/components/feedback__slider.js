window.cust.feedback__slider.slick({
	dots: false,
	infinite: false,
	speed: 300,
	slidesToShow: 1,
	slidesToScroll: 1,
	adaptiveHeight: true,
	prevArrow: '<button type="button" class="slick-prev"><svg width="1em" height="1em" class="icon icon-hex-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-hex-left"></use></svg></button>',
	nextArrow: '<button type="button" class="slick-next"><svg width="1em" height="1em" class="icon icon-hex-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-hex-right"></use></svg></button>'
});