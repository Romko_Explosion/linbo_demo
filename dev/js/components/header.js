var body = $('body'),
	header = $('.c-header'),
	footer = $('.c-footer');

body.on('click', function (e) {
	if($(e.target).closest('.c-header__location-trig, .c-header__location-drop').length === 0){
		window.cust.hideBlock($('.c-header__location-trig'), $('.c-header__location-drop'));
	};
	if($(e.target).closest('.c-header__phone-trig, .c-header__phone-drop').length === 0){
		window.cust.hideBlock($('.c-header__phone-trig'), $('.c-header__phone-drop'));
	};
	if($(e.target).closest('.c-header__login-group-trig, .c-header__login-group').length === 0){
		window.cust.hideBlock($('.c-header__login-group-trig'), $('.c-header__login-group'));
	};
	if($(e.target).closest('.c-settings__sidebar-trig, .c-settings__sidebar').length === 0){
		window.cust.hideBlock($('.c-settings__sidebar-trig'), $('.c-settings__sidebar'));
	};
});

$('.modal').on('show.bs.modal', function (e) {
	if (window.cust.is_scroll() && window.cust.breakPoint != 'xs' && window.cust.breakPoint != 'xx') {
		footer.css({
			'right': window.cust.getScrollBarWidth()
		});
	};
	if (window.cust.is_scroll() && (header.hasClass('c-header_white_fixed') || window.cust.breakPoint == 'xx' || window.cust.breakPoint == 'xs')) {
		header.css({
			'right': window.cust.getScrollBarWidth()
		});
	};
});

$('.modal').on('hidden.bs.modal', function (e) {
	footer.add(header).css({
		'right': 0
	});
});