var bower_path = '../../bower_components/';

requirejs.config({
    baseUrl: 's/js',  // base path with all js
    paths: {
        //alias to plugins
        domReady:            'lib/domReady',
        ymaps:               'https://api-maps.yandex.ru/2.1.48/?lang=ru_RU',
        modernizr:           'lib/modernizr-custom',
        detectmobilebrowser: 'lib/detectmobilebrowser',
        // libs
        jquery:               bower_path +'jquery/dist/jquery', // lib for js
        // plugins
        svg4everybody:        bower_path + 'svg4everybody/dist/svg4everybody.min',  // load svg
        scrollbar:            bower_path + 'perfect-scrollbar/js/perfect-scrollbar.jquery.min',
        slick:                bower_path + 'slick-carousel/slick/slick.min',
        bs_select:            bower_path + 'bootstrap-select/dist/js/bootstrap-select',
        bs_dropdown:          bower_path + 'bootstrap/js/dropdown',
        bs_collapse:          bower_path + 'bootstrap/js/collapse',
        bs_modal:             bower_path + 'bootstrap/js/modal',
        bs_tab:               bower_path + 'bootstrap/js/tab',
        bs_alert:             bower_path + 'bootstrap/js/alert',
        bs_transition:        bower_path + 'bootstrap/js/transition',
        svg4everybody:        bower_path + 'svg4everybody/dist/svg4everybody.min',
        jquery_validate:      bower_path + 'jquery-validation/dist/jquery.validate.min',
        mask:                 bower_path + 'jquery-mask-plugin/dist/jquery.mask.min',
        selectize:            bower_path + 'selectize/dist/js/selectize.min',
        sifter:               bower_path + 'sifter/sifter.min',
        microplugin:          bower_path + 'microplugin/src/microplugin',
        // separate
        separate_global:     'separate/global', // detect width && height of window
        // helpers
        toggle_blocks:       'helpers/toggle-blocks',
        detect_browser:      'helpers/detect_browser',
        get_scrollbar_width: 'helpers/get_scrollbar_width',
        resizer:             'helpers/resizer',
        object_fit:          'helpers/object-fit',
        bs_modal_center:     'helpers/bsModalCenter',
        bs_modal_fix:        'helpers/bs_modal_fix',
        valid:               'helpers/valid',
        // components
        header:              'components/header',
        init_maps:           'components/init_maps',
        contact_map:         'components/contact_map',
        feedback__slider:    'components/feedback__slider',
        item_slider:         'components/item-slider',
        advertises_slider:   'components/advertises-slider',
        detail_slider:       'components/detail-slider',
        c_search_store:      'components/c-search-store',
        c_video:             'components/c-video',
        c_collapse:          'components/collapse',
        c_settings:          'components/settings'
    }
});
var main_js_components = [
    'separate_global',
    'modernizr',
    'toggle_blocks',
    'detect_browser',
    'get_scrollbar_width',
    'resizer'
];
requirejs(['domReady','jquery'], function(domReady, $){
    // domReady(function () {
    //     // console.log("DOM Ready");
    // });
    requirejs(main_js_components, function() {
        requirejs(['header','object_fit','bs_alert']);
        requirejs(['bs_dropdown','bs_select'], function(){
            $('.selectpicker').selectpicker();
        });
        if ($('.js--slick').length > 0) {
            requirejs(['slick'], function (e) {
                window.cust.feedback__slider = $('.c-feedback__slider');
                if (window.cust.feedback__slider.length > 0) {
                    requirejs(['feedback__slider']);
                };
                window.cust.item_slider = $('.c-search-results__item-slider');
                if (window.cust.item_slider.length > 0) {
                    requirejs(['item_slider']);
                };
                window.cust.advertises_slider = $('.c-advertises__list');
                if (window.cust.advertises_slider.length > 0) {
                    requirejs(['advertises_slider']);
                };
                window.cust.detail_slider = $('.c-detail__slider');
                window.cust.detail_nav_slider = $('.c-detail__nav-slider');
                if (window.cust.detail_slider.length > 0 && window.cust.detail_nav_slider.length > 0) {
                    requirejs(['detail_slider']);
                };
            });
        };
        if($('[data-mask]').length > 0) {
            requirejs(['mask'], function () {
               $('[data-mask]').each(function() {
                    var th = $(this);
                    th.mask(th.attr('data-mask'));
                });
            });
        };
        if ($('a[data-toggle="tab"]').length > 0) {
            requirejs(['bs_tab', 'bs_transition']);
        };
        if ($('.collapse').length > 0) {
            requirejs(['c_collapse']);
        };
        if($('.js--phone-wrap').length > 0) {
            requirejs(['c_search_store']);
        };
        if($('.c-video').length > 0) {
            requirejs(['c_video']);
        };
        if($('.c-settings').length > 0) {
            requirejs(['c_settings']);
        };
        if($('.form_check').length > 0) {
            requirejs(['valid']);
        };
        if($('.js-selectize').length > 0) {
            requirejs(['selectize'], function () {
                $('.js-selectize').selectize({
                    delimiter: ' ',
                    persist: false,
                    create: function(input) {
                        return {
                            value: input,
                            text: input
                        }
                    }
                });
            });
        };
        if($('.modal').length > 0) {
            requirejs(['bs_modal','bs_transition'], function () {
                requirejs(['bs_modal_fix']);
                if($('.modal_centered').length > 0) {
                    requirejs(['bs_modal_center']);
                };
            });
        };
        if($('.js--map').length > 0) {
            requirejs(['ymaps'], function () {
                requirejs(['init_maps']);
            });
        };
        requirejs(['svg4everybody'], function () {
            svg4everybody();
        });
    });
});